package Drivers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DSL {
	
	private WebDriver driver;

	public DSL(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public void escrever(String id_campo,String texto) {
		driver.findElement(By.id(id_campo)).sendKeys(texto);
	}
	
	public void clicar(String id_btn) {
		driver.findElement(By.id(id_btn)).click();
	}
	
	public String obterTexto(String id_campo) {
		return driver.findElement(By.id(id_campo)).getText();
	}
	
	public String obterAtributo(String id_atributo, String valor) {
		return driver.findElement(By.id(id_atributo)).getAttribute(valor);
	}
}
