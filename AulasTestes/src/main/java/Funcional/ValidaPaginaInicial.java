package Funcional;

import org.junit.Test;

import Drivers.DSL;
import junit.framework.Assert;

public class ValidaPaginaInicial{

	private DSL dsl;
	
	@Test
	public void loginSucesso() throws InterruptedException {
		dsl.escrever("txtUsername", "Admin");
		dsl.escrever("txtPassword", "admin123");
		dsl.clicar("btnLogin");
		Thread.sleep(2000);
		
		String pginicial = dsl.obterTexto("welcome");
		Assert.assertEquals("Welcome Admin", pginicial);
		
	}
}
